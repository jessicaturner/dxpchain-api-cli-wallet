# Dxpchain API via CLI Wallet
### Examples using:
+ [cURL (PHP)](http://php.net/manual/en/book.curl.php)
+ [Websocket (Javascript)](https://developer.mozilla.org/en-US/docs/Web/API/WebSocket)
+ [JsonRPCClient (PHP)](https://gist.github.com/banyan/308599)

The examples shows how you can interface with Dxpchain' Wallet API. 

Environment and Requirements
+ Dxpchain v2
+ Dxpchain v2 CLI Tools
+ Full Node

Tested using Genesis File setup: http://docs.dxpchain.org/testnet/private-testnet.html. 

You can setup your own RPC endpoint by following the tutorial from this link: http://docs.dxpchain.org/integration/apps/cliwallet.html

You can read more about Dxpchain from this link: http://docs.dxpchain.org/integration/exchanges/index.html

If you are looking for an example on how to interface with Dxpchain' **Blockchain API** - [click here](https://gitlab.com/jessicaturner/dxpchain-api-php)
