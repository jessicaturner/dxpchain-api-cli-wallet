<?php 
/**
 * Dxpchain Wallet API connection using cURL
 *
 * @author:			Carl Victor C. Fontanos
 * @author_url: 	www.carlofontanos.com
 */
 
 
$url = 'ws://127.0.0.1:11011';
$array = array(
    'jsonrpc'   => '2.0',
    'method'    => 'get_account_history',
    'params'    => ['matrix', 10],
    'id'        => 1
);


$ch = curl_init();
curl_setopt($ch, CURLOPT_USERAGENT, 'GraphenePHP/1.0');
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($array));
$response = curl_exec($ch);
curl_close($ch);

echo '<pre>';
print_r($response);
echo '</pre>'; 
